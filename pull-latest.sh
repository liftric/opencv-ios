#!/bin/bash
git clone https://github.com/opencv/opencv.git

cd opencv
git checkout 4.7.0

OPENCV_SKIP_XCODEBUILD_FORCE_TRYCOMPILE_DEBUG=1 python3 platforms/apple/build_xcframework.py \
    --iphoneos_archs arm64 \
    --iphonesimulator_archs x86_64,arm64 \
    --disable-bitcode True \
    --build_only_specified_archs \
    --out ../build_xcframework \
    --without legacy \
    --without java \
    --without js \
    --without python2 \
    --without python3 \
    --without calib3d \
    --without dnn \
    --without features2d \
    --without flann \
    --without gapi \
    --without highgui \
    --without imgcodec \
    --without ml \
    --without objc \
    --without objdetect \
    --without photo \
    --without stitching \
    --without video \
    --without videoio \
    --without world
