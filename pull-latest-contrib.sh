#!/bin/bash
git clone https://github.com/opencv/opencv.git
git clone https://github.com/opencv/opencv_contrib.git

cd opencv
git checkout 4.7.0
cd ../opencv_contrib
git checkout 4.7.0
cd ../opencv

OPENCV_SKIP_XCODEBUILD_FORCE_TRYCOMPILE_DEBUG=1 python3 platforms/apple/build_xcframework.py \
    --iphoneos_archs arm64 \
    --iphonesimulator_archs x86_64,arm64 \
    --disable-bitcode True \
    --build_only_specified_archs \
    --out ../build_xcframework \
    --contrib ../opencv_contrib \
    --without legacy \
    --without java \
    --without js \
    --without python2 \
    --without python3 \
    --without dnn \
    --without gapi \
    --without highgui \
    --without imgcodec \
    --without ml \
    --without objc \
    --without photo \
    --without stitching \
    --without video \
    --without videoio \
    --without world \
    --without alphamat \
    --without barcode \
    --without bgsegm \
    --without bioinspired \
    --without ccalib \
    --without cnn_3dobj \
    --without cudaarithm \
    --without cudabgsegm \
    --without cudacodec \
    --without cudafeatures2d \
    --without cudafilters \
    --without cudaimgproc \
    --without cudalegacy \
    --without cudaobjdetect \
    --without cudaoptflow \
    --without cudastereo \
    --without cudawarping \
    --without cudev \
    --without cvv \
    --without datasets \
    --without dnn_objdetect \
    --without dnn_superres \
    --without dnns_easily_fooled \
    --without dpm \
    --without face \
    --without freetype \
    --without fuzzy \
    --without hdf \
    --without hfs \
    --without img_hash \
    --without intensity_transform \
    --without julia \
    --without line_descriptor \
    --without matlab \
    --without mcc \
    --without optflow \
    --without ovis \
    --without phase_unwrapping \
    --without plot \
    --without quality \
    --without rapid \
    --without reg \
    --without rgbd \
    --without saliency \
    --without sfm \
    --without shape \
    --without stereo \
    --without structured_light \
    --without superres \
    --without surface_matching \
    --without text \
    --without tracking \
    --without videostab \
    --without viz \
    --without wechat_qrcode \
    --without xfeatures2d \
    --without ximgproc \
    --without xobjdetect \
    --without xphoto
