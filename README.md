# Prebuild binaries of OpenCV for iOS

This repository serves prebuild binaries of [OpenCV](https://opencv.org/) for iOS via
`opencv-ios.json`.

### Building Binaries
Use `sh ./pull-latest.sh` (you may have to change the version number inside the
script) to build an opencv binary.

### Using via Carthage
You can fetch these binaries via [Carthage](https://github.com/Carthage/Carthage)

```
binary
"https://git.preventis.io/idk.algore/opencv-ios/raw/master/opencv-ios.json" ==
3.3.0
```
