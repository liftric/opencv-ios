// swift-tools-version:5.4
import PackageDescription

let package = Package(
    name: "OpenCV",
    platforms: [
        .iOS(.v12)
    ],
    products: [
        .library(
            name: "opencv2",
            targets: ["opencv2"]
        )
    ],
    targets: [
        .binaryTarget(
            name: "opencv2",
            url: "https://gitlab.com/liftric/opencv-ios/-/raw/master/releases/xcframework/opencv-ios-4.7.0-contrib.zip",
            checksum: "ce8ce8b3156b97a115e4d48d6bc070d6cd63fea20ddf6c7eea8a254aaa321fe6"
        )
    ]
)
